package com.mycompany.l01;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TitlesTest extends FirstTest{

    @Test()
    public void runBrowser() {
        driver.get(googleUrl);
        actualTitle = driver.getTitle();
        driver.navigate().refresh();
        driver.navigate().back();
        driver.navigate().forward();

        if (expectedTitle.equals(actualTitle)) {
            System.out.println("test pass");
        }
        else {
            Assert.fail("test failed cause Titles are not equals");
        }

    }
}
